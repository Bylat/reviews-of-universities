from database import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.VARCHAR(50), nullable=False, unique=True)
    email = db.Column(db.VARCHAR(100), nullable=False, unique=True)
    password = db.Column(db.VARCHAR, nullable=False)


class University(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.VARCHAR(50), nullable=False, unique=True)
    short_description = db.Column(db.VARCHAR(600), nullable=False, unique=True)

class Feedback(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.VARCHAR(600), nullable=False, unique=True)
    university = db.Column(db.Text, db.ForeignKey('university.name'), nullable=False)


class Teachers(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.VARCHAR(50), nullable=False, unique=True)
    short_description = db.Column(db.VARCHAR(600), nullable=False, unique=True)
    university = db.Column(db.Text, db.ForeignKey('university.name'), nullable=False)