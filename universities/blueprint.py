from flask import Blueprint
from flask import render_template

feedbacks = Blueprint("feedbacks", __name__, template_folder='templates')


@feedbacks.route('/')
def index():
    return render_template('feedbacks/index.html')
