from flask import Flask
from config import Configuration
#from database import db
from universities.blueprint import feedbacks

app = Flask(__name__)
app.config.from_object(Configuration)

#with app.app_context():
   # db.create_all()


app.register_blueprint(feedbacks, url_prefix='/unic')
