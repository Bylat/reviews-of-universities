class Configuration(object):
    DEBUG = True
    SECRET_KEY = 'a really really really really long secret key'
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:pass@localhost/flask_app_db'